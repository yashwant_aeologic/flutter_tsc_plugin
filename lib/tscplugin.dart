import 'dart:async';

import 'package:flutter/services.dart';

class Tscplugin {
  static const MethodChannel _channel =
      const MethodChannel('tscplugin');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }
}
